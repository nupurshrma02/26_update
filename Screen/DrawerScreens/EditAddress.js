import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Button, TouchableOpacity, Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import {Appbar, TextInput} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
const EditAddress = (navigation,route) => {
    const { itemId } = encodeURIComponent(route.params)
    const { itemPincode } = encodeURI(route.params)
    const {itemCity}   = encodeURIComponent(route.params)
    const {itemFn}    = encodeURIComponent(route.params)
    const {itemLn}    = encodeURIComponent(route.params)
    const {itemAddress}   = encodeURIComponent(route.params)
    const {itemLandmark}    = encodeURIComponent(route.params)
    const {itemMobile}    = encodeURIComponent(route.params)

  
  const [firstname, setFirstname] = useState(itemFn);
  const [lastname, setLastname] = useState(itemLn);
  const [Pincode, setPincode] = useState(false);
  const [city, setCity] = useState(itemCity);
  const [Address, setAddress] = useState(itemAddress);
  const [Landmark, setLandmark] = useState(itemLandmark);
  const [mobile, setMobile] = useState(itemMobile);

  useEffect(() => {
    if (itemPincode) {
        setPincode(true)
    }
   }, []);

    
  
    //   setFirstname(itemFn),
    //   setLastname(itemLn),
    //   setPincode(itemPincode),
    //   setCity(itemCity),
    //   setAddress(itemAddress),
    //   setLandmark(itemLandmark),
    //   setMobile(itemMobile)
  

//    useEffect( async() =>{
//     try {
//         let user = await AsyncStorage.getItem('userdetails');
//         user = JSON.parse(user);
//         setUserid(user.id)
//         //console.log('user id=>', user.id)
//         //console.log('userData =>', user);
//       } catch (error) {
//         console.log(error);
//       }
//   });


  const myfun = async() => {
    let user = await AsyncStorage.getItem('userdetails');
    user = JSON.parse(user);
    //let user_id= setUserid(user.id);
    console.log('user id=>', user.id)
    //Alert.alert(petname);
    //let user_id = {setUserid}
    let dataFetchUrl = 'http://3.12.158.241/medical/api/storeAddress/'+user.id;
    console.log('Addressurl---->', dataFetchUrl);
    await fetch(dataFetchUrl,{
        method : 'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "pincode": Pincode,
          "city_state": city,
          "address": Address,
          "landmark": Landmark,
          "first_name":firstname,
          "last_name" :lastname,
          "mobile": mobile,
                      
     })
    }).then(res => res.json())
    .then(resData => {
       console.log(resData);
       //Alert.alert(resData.message);
       if(resData.msg === 'User Details Created'){
        alert('Address Added Successfully')
        navigation.navigate('deliveryAddressStack')
       }
    });

    console.log('pincode--->',itemPincode);
 }
  

return(
  <ScrollView style={styles.contentBody}>
  
  <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
      <TextInput
          label={setPincode(true)}
          value={Pincode}
          onChangeText={(Pincode) => setPincode(Pincode)}
          style={[styles.textInputall, styles.widtthirty]}
          />
      <TextInput
          label="city/state"
          value={city}
          onChangeText={(e) => setCity(e.target.value)}
          style={[styles.textInputall, styles.widtsuxtysix]}
          />
  </View>
  <Text/>
  <Text/>
  <TextInput
      label="First Name"
      value={firstname}
      onChangeText={(e) => setFirstname(e.target.value)}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label="Last Name"
      value={lastname}
      onChangeText={(e) => setLastname(e.target.value)}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label="Address"
      value={Address}
      onChangeText={(e) => setAddress(e.target.value)}
      multiline = {true}
      numberOfLines = {5}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label="Landmark"
      value={Landmark}
      onChangeText={(e) => setLandmark(e.target.value)}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label="Mobile No."
      value={mobile}
      onChangeText={(e) => setMobile(e.target.value)}
      style={styles.textInputall}
      />
  
  <Text/>
  <View style={{alignItems: 'center'}}>
  <TouchableOpacity onPress={myfun} style={styles.appButtonContainer}>
         <Text style={styles.appButtonText}>Update</Text>
      </TouchableOpacity>
  </View>
  <Text/>
  </ScrollView>
);
}
export default EditAddress;

const styles = StyleSheet.create({
  image: {
      width: '100%'
  },
  contentBody: {
      paddingHorizontal: 15,
      paddingTop:10,
  },
  textInputall: {
      backgroundColor: '#fff'
  },
  widtthirty: {
      width: '30%',
  },
  widtfortyet: {
      width: '48%',
  },
  radios: {
      flexDirection: 'row',
      justifyContent: 'space-evenly'
  },
  widtsuxtysix: {
      width: '66%',
  },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#1e90ff",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginTop: 60,
    width: '50%',
    marginLeft: 10,
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  },
});